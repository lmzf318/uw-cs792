package bbm.uw.com.simbbm.constant;

/**
 * Created by jiy on 4/4/16.
 */
public class Constants {
    public final static String API_GET_URL = "/schedule/get/";
    public final static String API_BASE_URL = "http://10.0.2.2:9010";// 10.0.2.2 equals localhost on emulator
//    public final static String API_BASE_URL = "http://127.0.1.1:9010";

    // for handlers
    public final static int MESSAGE_GET_SCHEDULE_SUCCESS = 10100;
    public final static int MESSAGE_GET_SCHEDULE_FAIL = 10101;
}
